//include: header.h//


vec3 permute(in vec3 x) { return mod( x*x*34.+x, 289.); }
float snoise(in vec2 v) {
    vec2 i = floor((v.x+v.y)*.36602540378443 + v),
        x0 = (i.x+i.y)*.211324865405187 + v - i;
    float s = step(x0.x,x0.y);
    vec2 j = vec2(1.0-s,s),
        x1 = x0 - j + .211324865405187,
        x3 = x0 - .577350269189626;
    i = mod(i,289.);
    vec3 p = permute( permute( i.y + vec3(0, j.y, 1 ))+ i.x + vec3(0, j.x, 1 )   ),
        m = max( .5 - vec3(dot(x0,x0), dot(x1,x1), dot(x3,x3)), 0.),
        x = fract(p * .024390243902439) * 2. - 1.,
        h = abs(x) - .5,
        a0 = x - floor(x + .5);
    return .5 + 65. * dot( pow(m,vec3(4.))*(- 0.85373472095314*( a0*a0 + h*h )+1.79284291400159 ), a0 * vec3(x0.x,x1.x,x3.x) + h * vec3(x0.y,x1.y,x3.y));
}

const float mult = 48;
void main() {
    float v = snoise((uv_coords + vec2(12.1,0))*mult);
    float w = snoise((uv_coords + vec2(0,51.4))*mult);
    float z = snoise((uv_coords + vec2(-23.1,3.2))*mult);
    float q = snoise((uv_coords + vec2(-9.2,-71.2))*mult);

    output_color = vec4(v,w,z,q)*.125 + 0.5;

    // snoise(vec2(uv_coords.x, ubo.time*0.1)*40),
    // snoise(vec2(ubo.time*0.1, uv_coords.y)*40),
    // snoise(vec2(ubo.time/10.0 + uv_coords.x * 2 + 5.1293, -uv_coords.x * 2 + ubo.time/10.0)*100),
    // snoise(vec2(ubo.time/10.0 + uv_coords.y * 2 - 9.1293, -12.45 + uv_coords.x * 2 + ubo.time/10.0)*100));
}
