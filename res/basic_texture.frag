//include: header.h//
//include: rng.frag//


vec3 permute(in vec3 x) {
    return mod( x*x*34.+x, 289.);
}

float snoise(in vec2 v, in vec2 seed) {
    vec2 i = floor((v.x+v.y)*.36602540378443 + v),
        x0 = (i.x+i.y)*.211324865405187 + v - i;
    float s = step(x0.x,x0.y);
    vec2 j = vec2(1.0-s,s),
        x1 = x0 - j + .211324865405187,
        x3 = x0 - .577350269189626;
    i = mod(i+seed,289.);
    vec3 p = permute( permute( i.y + vec3(0, j.y, 1 ))+ i.x + vec3(0, j.x, 1 )   ),
        m = max( .5 - vec3(dot(x0,x0), dot(x1,x1), dot(x3,x3)), 0.),
        x = fract(p * .024390243902439) * 2. - 1.,
        h = abs(x) - .5,
        a0 = x - floor(x + .5);
    return .5 + 65. * dot( pow(m,vec3(4.))*(- 0.85373472095314*( a0*a0 + h*h )+1.79284291400159 ), a0 * vec3(x0.x,x1.x,x3.x) + h * vec3(x0.y,x1.y,x3.y));
}


// Returns [iso, d, p.x, p.y]
//
// iso: distance from cell border
// d: distance from cell center
// p: cell center position
vec4 voronoi(vec2 p, float t) {
    vec2 ic = floor(p);
    vec2 uc = fract(p);

    vec2 mg, mr;
    float md = 16;

    for(int dx = -1; dx <= 1; dx++) {
        for(int dy = -1; dy <= 1; dy++) {
            vec2 dc = vec2(dx,dy);
            vec4 h = hash24(ic + dc);
            vec2 cr = 0.5 + 0.5 * sin(TAU * (h.xy*t + h.zw));
            vec2 r = dc + cr - uc;
            float d = length(r);

            if (d<md) {
                md = d;
                mr = r;
                mg = dc;
            }
        }
    }

    float iso = 16;
    for(int dx = -2; dx <= 2; dx++) {
        for(int dy = -2; dy <= 2; dy++) {
            if (dx != 0 || dy != 0) {
                vec2 dc = mg + vec2(dx,dy); // 373
                vec4 h = hash24(ic + dc);
                vec2 cr = 0.5 + 0.5 * sin(TAU * (h.xy*t + h.zw));
                vec2 r = dc + cr - uc;
                iso = min(iso, dot(0.5*(mr+r), normalize(r-mr)));
            }
        }
    }

    return vec4(iso, md, mr);
}

float sharpnoise(in vec2 v, in vec2 seed) {
    float r = snoise(v, seed);
    return smoothstep(0, 1, r);
}

void main() {
    vec2 uv = uv_coords;

    uv += pc.input0.xy;
    uv *= pc.input0.zw;

    // vec3 tot = vec3(0);
    // vec3 col = vec3(0.1,0.1,0.1);

    // col += vec3(0.5,0.2,0.0) * sharpnoise(uv*3*2, vec2(1,2.2)+s);
    // col += vec3(0.2,0.2,0.1) * sharpnoise(uv*1*2, vec2(3,2.2)+s);
    // col += vec3(0.1,0.2,0.2) * sharpnoise(uv*5*2, vec2(4,2.2)+s);

    // float sharp = snoise(uv, pc.input1.xy*128+s);

    // col = smoothstep(0.1, 0.25+sharp*3, col);

    vec2 brd = fract(uv+0.5)-0.5;
    brd = step(0.0001, brd*brd);

    vec3 col = vec3(0,0,0);
    float t = pc.time*0.8;
    // t = floor(t) + pow(smoothstep(0.5,1,fract(t)),4);

    float c = 1;
    for(int oct = 0; oct < 6; oct++) {
        float q = voronoi(hash22(vec2(c)) * 100 + uv*c, t*0.5).x;
        col = mix(col, vec3(oct/16.0+q), 0.2);
        c *= 2;
    }

    output_color = vec4(col, 1);
}
