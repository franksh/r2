#version 460 core

// Uniform data given to each pipeline.
layout(set=0, binding=0) uniform uniformBlock {
    float time; // time (in seconds) since some arbitrary point (application start).
    float frame_delta; // delta (in seconds) since last render pass.
    uint frame_index; // 0-indexed frame number.
    float aspect_ratio; // output texture aspect ratio.
    ivec2 resolution; // output texture size.
    vec4 frame_noise; // 4x random numbers in [0,1] (random each frame).
    vec2 mouse_position;
    vec2 mouse_velocity;
} ubo;

layout(push_constant) uniform pushConstants {
    float time;
} upc;

// Every pipeline gets a linear & a nearest sampler.
layout(set=0, binding=1) uniform sampler nearestSampler;
layout(set=0, binding=2) uniform sampler linearSampler;

// Texture inputs to shader.
layout(set=0, binding=3) uniform texture2D lastFrame;


// texture coords of quad.
layout(location=0) in vec2 uv_coords;

// color output.
layout(location=0) out vec4 output_color;


const float PI = 3.1415926535897932384626433832795;
const float TAU = 6.2831853071795864769252867665590;

vec2 rot(float t, vec2 p) {
    return p * mat2(cos(t), -sin(t), sin(t), cos(t));
}

void main() {
    if (ubo.frame_index < 10) {
        float z = smoothstep(0.15, 0.155, length(uv_coords-0.5));
        float x = uv_coords.x;
        float y = uv_coords.y;
        output_color = vec4(z*(1-x)*y, z*x*y, z*x*(1-y), 1);
        return;
    }

    float t1 = cos(1.54 * ubo.time) * 0.5 + 0.5;
    float t2 = cos(2.27 * ubo.time) * 0.5 + 0.5;

    vec2 c = (uv_coords - 0.5) * (1 + (t1-0.4)/80.0);
    c = rot((2+0.4*t2-1.4*length(c))/150,c);

    vec4 q = texture(sampler2D(lastFrame, linearSampler), clamp(c + 0.5, 0, 1));
    if (length(2*c) > 1.2) {
        float x = uv_coords.x;
        float y = uv_coords.y;
        vec4 Q = vec4((1-x)*y, x*y, x*(1-y), 1);
        q = mix(q,Q,0.08);
    }

    output_color = q;
}

// float dot2(in vec3 v ) { return dot(v,v); }

// float sdRoundCone(vec3 p, vec3 a, vec3 b, float r1, float r2) {
//     // sampling independent computations (only depend on shape)
//     vec3  ba = b - a;
//     float l2 = dot(ba,ba);
//     float rr = r1 - r2;
//     float a2 = l2 - rr*rr;
//     float il2 = 1.0/l2;

//     // sampling dependant computations
//     vec3 pa = p - a;
//     float y = dot(pa,ba);
//     float z = y - l2;
//     float x2 = dot2( pa*l2 - ba*y );
//     float y2 = y*y*l2;
//     float z2 = z*z*l2;

//     // single square root!
//     float k = sign(rr)*rr*rr*x2;
//     if( sign(z)*a2*z2 > k ) return  sqrt(x2 + z2)        *il2 - r2;
//     if( sign(y)*a2*y2 < k ) return  sqrt(x2 + y2)        *il2 - r1;
//                             return (sqrt(x2*a2*il2)+y*rr)*il2 - r1;
// }


// float map( in vec3 pos )
// {
//     // aniamte cone
//     #if 1
//     vec3  pa = vec3(-0.1,-0.1,0.1);
//     vec3  pb = vec3( 0.4, 0.3,0.3);
//     float ra = 0.3;
//     float rb = 0.1;
//     #else
//     vec3  pa = 0.4*cos(iTime*vec3(1.0,1.3,0.8)+vec3(0.0,2.0,4.0));
//     vec3  pb = 0.4*cos(iTime*vec3(0.9,1.1,0.7)+vec3(1.0,3.0,6.0));
//     float ra = 0.2 + 0.1*sin(iTime*1.7+0.0);
//     float rb = 0.2 + 0.1*sin(iTime*1.8+2.0);
//     #endif


//     return sdRoundCone(pos, pa, pb, ra, rb );
// }

// // http://iquilezles.org/www/articles/normalsSDF/normalsSDF.htm
// vec3 calcNormal( in vec3 pos )
// {
//     vec2 e = vec2(1.0,-1.0)*0.5773;
//     const float eps = 0.0005;
//     return normalize( e.xyy*map( pos + e.xyy*eps ) +
// 					  e.yyx*map( pos + e.yyx*eps ) +
// 					  e.yxy*map( pos + e.yxy*eps ) +
// 					  e.xxx*map( pos + e.xxx*eps ) );
// }

// #define AA 3

// void mainImage( out vec4 fragColor, in vec2 fragCoord )
// {
//      // camera movement
// 	float an = 0.5*(iTime-10.0);
// 	vec3 ro = vec3( 1.0*cos(an), 0.4, 1.0*sin(an) );
//     vec3 ta = vec3( 0.0, 0.0, 0.0 );
//     // camera matrix
//     vec3 ww = normalize( ta - ro );
//     vec3 uu = normalize( cross(ww,vec3(0.0,1.0,0.0) ) );
//     vec3 vv = normalize( cross(uu,ww));



//     vec3 tot = vec3(0.0);

//     #if AA>1
//     for( int m=0; m<AA; m++ )
//     for( int n=0; n<AA; n++ )
//     {
//         // pixel coordinates
//         vec2 o = vec2(float(m),float(n)) / float(AA) - 0.5;
//         vec2 p = (-iResolution.xy + 2.0*(fragCoord+o))/iResolution.y;
//         #else
//         vec2 p = (-iResolution.xy + 2.0*fragCoord)/iResolution.y;
//         #endif

// 	    // create view ray
//         vec3 rd = normalize( p.x*uu + p.y*vv + 1.5*ww );

//         // raymarch
//         const float tmax = 2.0;
//         float t = 0.0;
//         for( int i=0; i<256; i++ )
//         {
//             vec3 pos = ro + t*rd;
//             float h = map(pos);
//             if( h<0.0001 || t>tmax ) break;
//             t += h;
//         }


//         // shading/lighting
//         vec3 col = vec3(0.0);
//         if( t<tmax )
//         {
//             vec3 pos = ro + t*rd;
//             vec3 nor = calcNormal(pos);
//             float dif = clamp( dot(nor,vec3(0.57703)), 0.0, 1.0 );
//             float amb = 0.5 + 0.5*dot(nor,vec3(0.0,1.0,0.0));
//             col = vec3(0.2,0.3,0.4)*amb + vec3(0.8,0.7,0.5)*dif;
//         }

//         // gamma
//         col = sqrt( col );
// 	    tot += col;
//     #if AA>1
//     }
//     tot /= float(AA*AA);
//     #endif

// 	fragColor = vec4( tot, 1.0 );
// }
