//include: header.h//



const float border_width = 3.0;

vec3 fractal(vec2 z) {
    vec2 dist = vec2(1.0e10);
    for (int i = 0; i < 200; i++) {
        z = vec2(z.x*z.x - z.y*z.y, 2*z.x*z.y);
        z -= (ubo.custom.xy - 0.5) * 2;

        float l = length(z);
        if (l > 20.0) break;

        vec2 d = max(vec2(-1), z - (ubo.custom.zw - 0.5)*2);
        dist = min(dist, d*d);
    }

    return vec3(pow(dist.x, 0.5), pow(dist.y, 0.5), abs(dist.x*dist.y));
}

void main() {
    float aspect = float(ubo.resolution.x) / float(ubo.resolution.y);
    vec2 uv = gl_FragCoord.xy / ubo.resolution.y;

    uv = (uv - 0.5) * 3;

    vec3 orb = fractal(uv);

    output_color = vec4(orb, 1);
}
