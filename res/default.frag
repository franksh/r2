#version 450 core

layout(set=0, binding=0)
uniform Uniforms_block_0Fs {
    float time;
    float delta;
    vec2 resolution;
} U;

layout(set=0, binding=1) uniform texture2D lastFrame;
layout(set=0, binding=2) uniform sampler frameSampler;

const float PI = 3.1415926535897932384626433832795;
const float TAU = 6.2831853071795864769252867665590;

// texture coords of quad.
layout(location=0) in vec2 iUV;
// layout(location=1) in vec2 iUV;

// color output.
layout(location=0) out vec4 oColor;


const float SHIFT23 = 1.1920928955078125e-07; // 2^-23

float hash21(vec2 p) {
    uint x = floatBitsToUint(p.x);
    uint y = floatBitsToUint(p.y);
    y ^= 0xbeefu;
    x ^= y << 7;
    x *= 0xC2B2AE3Du;
    y ^= x >> 8;
    y *= 0x85EBCA77u;
    uint h = (y) >> 9;
    return float(h) * SHIFT23;
}

vec2 hash22(vec2 p) {
    uint x = floatBitsToUint(p.x);
    uint y = floatBitsToUint(p.y);
    y ^= 0xdeadu;
    x ^= y << 7u;
    x *= 0xC2B2AE3Du;
    y ^= x >> 8u;
    y *= 0x85EBCA77u;
    x ^= y << 3u;
    x *= 0x34b020d1u;
    uint h0 = y >> 9;
    uint h1 = x >> 9;
    return vec2(float(h0), float(h1)) * SHIFT23;
}

vec2 hash32(vec3 p) {
    uint x = floatBitsToUint(p.x);
    uint y = floatBitsToUint(p.y);
    uint z = floatBitsToUint(p.z);
    y ^= 0xBABEu;
    x ^= y << 7u;
    x ^= z >> 13u;
    x *= 0xC2B2AE3Du;
    y ^= x >> 16u;
    y ^= z << 8u;
    y *= 0x85EBCA77u;
    z ^= y >> 6u;
    z ^= x << 3u;
    z *= 0x34b020d1u;
    uint h0 = z >> 9;
    uint h1 = (x^y) >> 9;
    return vec2(float(h0), float(h1)) * SHIFT23;
}


vec3 kernel2(vec2 v, vec2 p, float r) {
    float a = TAU * hash21(v);
    vec2 uv = vec2(cos(a), sin(a));

    vec3 rc = vec3(0.2 + 0.8*hash21(v), 0.2+0.8*hash21(200.1234*v+1), 0.2+0.8*hash21(v-1.238));

    float G = 0.5 - 1.0/(2*sqrt(3));
    vec2 s = v - (v.x + v.y) * G;
    vec2 dp = p - s; // p - s;
    float d = length(dp);

    vec2 l = dp * sqrt(2);
    float m = max(0, 1 - length(l));
    float g = dot(dp * sqrt(2./2.), uv);

    float S = pow(max(0,0.5-d*d), 4.0) * g * 86.95;

    if (abs(S) > 1.0) {
        return vec3(0,0,1);
    }
    if (abs(S) > 0.8) {
        return vec3(0,1,0);
    }
    return vec3(0.5*S+0.5);
    m *= m;

    return vec3(m * (g*0.5+0.5)); //* (g+0.5)*0.5;
    // return abs(pow(dot(uv, dp),1) * (1-l));
}

vec3 noise21(vec2 p, float seed) {
    float F = 0.5 * (sqrt(3) - 1);

    //   *---*
    //  / \ /
    // *---*

    vec2 v = p + F * (p.x + p.y);

    vec2 v_i = floor(v);
    vec2 v_u = fract(v); //v - v_i;

    float G = 0.5 - 1.0/(2*sqrt(3));
    vec2 cell = v_i - (v_i.x + v_i.y) * G;
    vec2 disp = p - cell;

    vec2 o = (v_u.x > v_u.y) ? vec2(1,0) : vec2(0,1);

    vec3 X = vec3(0);
    float c = 0.0;
    X += kernel2(v_i, p, seed);
    X += kernel2(v_i+o, p, seed);
    X += kernel2(v_i+1, p, seed);
    // X.r = kernel2(v_i, p, 0.5);
    // X.g = kernel2(v_i+o, p, 0.5);
    // X.b = kernel2(v_i+1, p, 0.5);
    // X=kernel2(v_i+vec2(0), p); c += pow(max(0,0.5-X.x), 0)*X.y;
    // X=kernel2(v_i+o, p); c += pow(max(0,0.6-X.x), 0)*X.y;
    // X=kernel2(v_i+vec2(1), p); c += pow(max(0,0.6-X.x), 0)*X.y;

    return X/3.;
    // c = pow(c+0.009,0.5);
    return vec3((X.r+X.g+X.b)/3.0);

}

void main2() {
    vec2 iResolution = vec2(1280,768);
    float iTime = U.time;
    vec2 fragCoord = gl_FragCoord.xy;
    fragCoord.y = iResolution.y - fragCoord.y;

    vec2 uv = 2 * (fragCoord.xy - iResolution/2) / iResolution.y;

    uv *= 1.6;
    // vec2 uv = 2 * UV - 1;

    uv += vec2(500,500) + vec2(100.0*sin(iTime*0.001), 300 * cos(iTime*0.0005));

    vec3 x = noise21(uv, iTime);
    oColor = vec4(x, 1);
}

vec4 permute(vec4 t) {
    return t * (t * 34.0 + 133.0);
}

// Gradient set is a normalized expanded rhombic dodecahedron
vec3 grad(float hash) {

    // Random vertex of a cube, +/- 1 each
    vec3 cube = mod(floor(hash / vec3(1.0, 2.0, 4.0)), 2.0) * 2.0 - 1.0;

    // Random edge of the three edges connected to that vertex
    // Also a cuboctahedral vertex
    // And corresponds to the face of its dual, the rhombic dodecahedron
    vec3 cuboct = cube;
    cuboct[int(hash / 16.0)] = 0.0;

    // In a funky way, pick one of the four points on the rhombic face
    float type = mod(floor(hash / 8.0), 2.0);
    vec3 rhomb = (1.0 - type) * cube + type * (cuboct + cross(cube, cuboct));

    // Expand it so that the new edges are the same length
    // as the existing ones
    vec3 grad = cuboct * 1.22474487139 + rhomb;

    // To make all gradients the same length, we only need to shorten the
    // second type of vector. We also put in the whole noise scale constant.
    // The compiler should reduce it into the existing floats. I think.
    grad *= (1.0 - 0.042942436724648037 * type) * 3.5946317686139184;

    return grad;
}

vec3 r_unitvec3(vec3 inp) {
    vec2 w = hash32(inp) * vec2(TAU, 2.0) - vec2(0,1);
    float r = sqrt(1-w.y*w.y);

    return vec3(r * cos(w.x), r * sin(w.x), w.y) * 8;
}

// BCC lattice split up into 2 cube lattices
vec4 openSimplex2SDerivativesPart(vec3 X) {
    vec3 b = floor(X);
    vec4 i4 = vec4(X - b, 2.5);

    // Pick between each pair of oppposite corners in the cube.
    vec3 v1 = b + floor(dot(i4, vec4(.25)));
    vec3 v2 = b + vec3(1, 0, 0) + vec3(-1, 1, 1) * floor(dot(i4, vec4(-.25, .25, .25, .35)));
    vec3 v3 = b + vec3(0, 1, 0) + vec3(1, -1, 1) * floor(dot(i4, vec4(.25, -.25, .25, .35)));
    vec3 v4 = b + vec3(0, 0, 1) + vec3(1, 1, -1) * floor(dot(i4, vec4(.25, .25, -.25, .35)));

    // Gradient hashes for the four vertices in this half-lattice.
    // vec4 hashes = permute(mod(vec4(v1.x, v2.x, v3.x, v4.x), 289.0));
    // hashes = permute(mod(hashes + vec4(v1.y, v2.y, v3.y, v4.y), 289.0));
    // hashes = mod(permute(mod(hashes + vec4(v1.z, v2.z, v3.z, v4.z), 289.0)), 48.0);

    vec3 g1 = r_unitvec3(v1);
    vec3 g2 = r_unitvec3(v2);
    vec3 g3 = r_unitvec3(v3);
    vec3 g4 = r_unitvec3(v4);

    // Gradient extrapolations & kernel function
    vec3 d1 = X - v1; vec3 d2 = X - v2; vec3 d3 = X - v3; vec3 d4 = X - v4;
    vec4 a = max(0.75 - vec4(dot(d1, d1), dot(d2, d2), dot(d3, d3), dot(d4, d4)), 0.0);
    vec4 aa = a * a; vec4 aaaa = aa * aa;
    // vec3 g1 = grad(hashes.x); vec3 g2 = grad(hashes.y);
    // vec3 g3 = grad(hashes.z); vec3 g4 = grad(hashes.w);
    // vec3 g1 = grad(h1.x); vec3 g2 = grad(h2.x);
    // vec3 g3 = grad(h3.x); vec3 g4 = grad(h4.x);
    vec4 extrapolations = vec4(dot(d1, g1), dot(d2, g2), dot(d3, g3), dot(d4, g4));

    // Derivatives of the noise
    vec3 derivative = -8.0 * mat4x3(d1, d2, d3, d4) * (aa * a * extrapolations)
        + mat4x3(g1, g2, g3, g4) * aaaa;

    // Return it all as a vec4
    return vec4(derivative, dot(aaaa, extrapolations));
}

float noise3(vec3 inp) {
    return openSimplex2SDerivativesPart(inp).w * 2;
}
vec3 noise33(vec3 inp) {
    return openSimplex2SDerivativesPart(inp).xyz;
}

float sdSphere( in vec3 p, in float r )
{
    return length(p)-r;
}

float sdBox( in vec3 p, in vec3 r )
{
    return length( max(abs(p)-r,0.0) ) - 0.01;
}

float smax( float a, float b, float k )
{
    float h = max(k-abs(a-b),0.0);
    return max(a, b) + h*h*0.25/k;
}

float sdFloor(vec3 p, float time) {
    float y = abs(p.y);
    float d = 1 - y;
    d += noise3(vec3(p.xz*0.9, 0.5*time)) / 4.0;
    return d;
}
float opSub( float d1, float d2 ) { return max(-d1,d2); }

float opSmoothSub( float d1, float d2, float k ) {
    float h = clamp( 0.5 - 0.5*(d2+d1)/k, 0.0, 1.0 );
    return mix( d2, -d1, h ) + k*h*(1.0-h);
}
float opSmoothMul( float d1, float d2, float k ) {
    float h = clamp( 0.5 - 0.5*(d2+d1)/k, 0.0, 1.0 );
    return mix( d2, d1, h ) + k*h*(1.0-h);
}

float sdSpace(vec3 p, float w, float time) {
    float q = 0.1+ 1*noise3(time*0.17+p*vec3(0.2,0.8,0.1));
    float d = 0.1 + 1*noise3(sin(p+time*0.1) * vec3(0.3,0.9,0.2));
    float d2 = 0.1+1*noise3(vec3(0,-100,0) + 0.5*p * vec3(0.5,0.6,0.2));
    d = opSmoothSub(d,q,0.1);
    d = opSmoothSub(d,d2,0.1);
    return opSmoothSub(w-7, d, 1);
}

vec3 floor_texture(vec3 pos) {
    vec3 col = vec3(0.3,0.3,0.1);

    col += sin(pos.x)*sin(pos.y)*sin(pos.z) / 4.0;
    col.r += 0.2 * sin(3*pos.y);
    col.g += smoothstep(0,3, 2 / (4*pos.x*pos.x + 1));
    col.b += 0.1 * sin(2*pos.y * pos.x);
    //float q = 2*noise3(u+time*0.1) + 0.5*noise3(9*u - time*0.4);

    // float v1 = 0.2*cos(pos.y/3);
    // float v2 = hash21(floor(pos.zz*10))*.4-0.2;
    // float v3 = hash21(floor(pos.xx*50 + pos.zz*20))-0.45;
    // col -= vec3(v1+v2);
    // col.b += 0.5*noise3(pos/3+vec3(-102,-32,21));
    // col.g *= 0.4+noise3(pos/5+2387);

    return clamp(col,0,1);
}


vec4 map( in vec3 rp, float time ) {
    vec3 p = rp - vec3(0,0,-2*time);
    vec3 u = normalize(p);
    // float d1 = sdSphere(p,0.5 + q/40.0);
    float d1 = sdSpace(p, length(rp), time);
    //float d2 = sdFloor(p, time);

    return vec4(d1, p);
}

vec3 calcNormal( in vec3 pos, in float time )
{
    vec3 n = vec3(0.0);
    for( int i=0; i<4; i++ )
    {
        vec3 e = 0.5773*(2.0*vec3((((i+3)>>1)&1),((i>>1)&1),(i&1))-1.0);
        n += e*map(pos+0.0005*e,time).x;
    }
    return normalize(n);
}

float calcAO( in vec3 pos, in vec3 nor, in float time )
{
	float occ = 0.0;
    float sca = 1.0;
    for( int i=0; i<5; i++ )
    {
        float h = 0.01 + 0.12*float(i)/4.0;
        h *= 0.5;
        float d = map( pos+h*nor, time ).x;
        occ += abs(h-d)*sca;
        sca *= 0.90;
    }
    return clamp( 1.0 - 3.0*occ, 0.0, 1.0 );
}

float calcSoftshadow( in vec3 ro, in vec3 rd, in float k, in float time )
{
    float res = 1.0;

    float tmax = 2.0;
    float t    = 0.001;
    for( int i=0; i<64; i++ )
    {
        float h = map( ro + rd*t, time ).x;
        res = min( res, k*h/t );
        t += clamp( h, 0.012, 0.2 );
        if( res<0.001 || t>tmax ) break;
    }

    return clamp( res, 0.0, 1.0 );
}

vec4 intersect( in vec3 ro, in vec3 rd, in float time )
{
    vec4 res = vec4(-1.0);

    float t = 0.001;
    float tmax = 100.0;
    for( int i=0; i<110 && t<tmax; i++ )
    {
        vec4 h = map(ro+t*rd,time);
        if( h.x < 0.001*t ) {
            res = vec4(t,h.yzw);
            break;
        }
        t += h.x;
    }

    return res;
}

mat3 setCamera( in vec3 ro, in vec3 ta, float cr )
{
	vec3 cw = normalize(ta-ro);
	vec3 cp = vec3(sin(cr), cos(cr),0.0);
	vec3 cu = normalize( cross(cw,cp) );
	vec3 cv =          ( cross(cu,cw) );
    return mat3( cu, cv, cw );
}

void main()
{
    vec2 iResolution = vec2(1280,768);
    float iTime = U.time;
    vec2 fragCoord = gl_FragCoord.xy;
    fragCoord.y = iResolution.y - fragCoord.y;
    vec3 tot = vec3(0.0);
    int AA = 2;

    for( int m=0; m<AA; m++ ) {
    for( int n=0; n<AA; n++ )
    {
        // pixel coordinates
        vec2 aa_offset = vec2(m,n) * (1.0 / float(AA)) - 0.5;
        vec2 p = (2*(fragCoord + aa_offset) - iResolution.xy)/iResolution.y;

        float time = iTime;

        // camera
        float an = TAU*time/10 ;
        vec3 ta = vec3( 0.0, 0.0, 1);
        vec3 ro = vec3( .5*cos(an), -0.1, 0 );

        // camera-to-world transformation
        mat3 ca = setCamera( ro, ta, 0.01*cos(an) );

        // ray direction
        float fl = 2.0;
        vec3 rd = ca * normalize( vec3(p,fl) );

        // background
        vec3 col = vec3(0);

        // raymarch geometry
        vec4 tuvw = intersect( ro, rd, time );
        float d = tuvw.x;
        vec3 pos = ro + d*rd;
        if( d>0.0 )
        {
            // shading/lighting
            vec3 nor = calcNormal(pos, time);

            float ao = calcAO(pos, nor, time);
            col = 0.2 + 0.5*nor;
            col += floor_texture(pos);
            col *= ao * ao;
        } else {
            d = 200.0;
        }
        col *= 8/(1+pow(d*d, sin(2*time)*0.1+1));


        // gamma
        // col = pow(col, vec3(0.45));
	    tot += col;
    } }
    tot /= float(AA*AA);

    vec2 tcoord = fragCoord/U.resolution;
    tcoord.y *= -1;
    vec3 pc = textureLod(sampler2D(lastFrame, frameSampler), tcoord, 0.0).xyz;

    pc = mix(tot, pc, 0.9);
    // tot = smoothstep(0.1, 0.2, tot);
    // cheap dithering
    // tot += sin(fragCoord.x*114.0)*sin(fragCoord.y*211.1)/512.0;

    oColor = vec4( pc, 1.0 );
}
