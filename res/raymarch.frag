
//include: header.h//
//include: rng.frag//
// line 305

const int max_march_steps = 100;
const float max_march_distance = 30.0;
const float hit_epsilon = 0.001;

struct HitData {
    bool is_hit;
    float dist;
    vec3 pos;
};

float map_distance(vec3 p, float t) {
    float s = length(p) - 0.5;

    float f = abs(p.y+1);

    float b = length(max(abs(p+vec3(0,0.75,0))-vec3(1,0.25,1), 0));

    return min(b,min(s,f));
}

vec3 map_normal(vec3 p, float t) {
    vec2 eps = vec2(0,hit_epsilon);

    return normalize(vec3(
        map_distance(p+eps.yxx, t) - map_distance(p-eps.yxx, t),
        map_distance(p+eps.xyx, t) - map_distance(p-eps.xyx, t),
        map_distance(p+eps.xxy, t) - map_distance(p-eps.xxy, t)));
}

HitData march(vec3 pos, vec3 dir, float time) {
    bool is_hit = false;
    float total = 0.0;
    float dist;
    for(int i = 0; i < max_march_steps; i++) {
        dist = map_distance(pos, time);
        total += dist;
        if(dist < hit_epsilon) {
            is_hit = true;
            break;
        }
        if (total > max_march_distance) {
            break;
        }
        pos += dir * dist;
    }

    return HitData(is_hit, dist, pos);
}

void main() {
    vec3 light_pos = vec3(3,1,8);
    vec3 camera_pos = pc.input0.xyz;
    vec3 lookat = vec3(0,0,0);
    vec3 world_up = vec3(0,1,0);
    float zoom = 1.1;

    vec3 camera_dir = normalize(lookat - camera_pos);
    vec3 camera_right = normalize(cross(world_up, camera_pos));
    vec3 camera_up = cross(camera_dir, camera_right);

    float aspect_ratio = pc.output_size.x / pc.output_size.y;
    vec2 uv = (2 * uv_coords - 1) * vec2(aspect_ratio, 1);

    vec3 ray_dir = normalize(camera_right * uv.x + camera_up * uv.y + zoom * camera_dir);

    HitData h = march(camera_pos, ray_dir, pc.time);

    vec3 color = vec3(0);

    if (h.is_hit) {
        vec3 normal = map_normal(h.pos, pc.time);

        float diffuse = max(0,dot(normalize(light_pos - h.pos), normal));

        float spec = pow(diffuse, 48);

        color += vec3(0,0.1,0.2);
        color += vec3(0.6,0.6,0.6) * diffuse;
        color += spec;
    }


    output_color = vec4(color, 1);
}
