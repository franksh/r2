//include: header.h//

float circle(vec2 p, float r) {
    return 1-smoothstep(r, r+2, distance(gl_FragCoord.xy, p));
}

float line(vec2 a, vec2 dir, float thickness) {
    vec2 p = gl_FragCoord.xy - a;
    float h = clamp(dot(p,dir) / dot(dir,dir), 0, 1);
    return 1-smoothstep(thickness, thickness+2, length(p - h * dir));
}

void main() {
    float v1 = circle(pc.click_pos, 10);
    float v2 = circle(pc.hover_pos, 10);
    float v3 = circle(pc.drag_target, 10);
    vec2 vel = pc.pointer_velocity;

    vec3 col = vec3(v1,v2,v3);
    col += vec3(1) * line(pc.output_size.xy/2, vel, 2.0);
    output_color = vec4(col, 1);
}
