#version 460 core

// Uniform data given to each pipeline.
layout(set=0, binding=0) uniform uniformBlock {
    float unused;
} ubo;

layout(push_constant) uniform pushConstants {
    // Position within timeline. It measures the number of seconds since some
    // arbitrary point. Could be negative.
    float time;

    // Current tick (frame) in timeline. Could be negative.
    int tick;

    // Size (in pixels) of output texture or surface. Usually to get uniform
    // "uv"-coordinates of the screen space you'd do
    // `gl_FragCoord/pc.output_size`, but this value is given directly from the
    // vertex shader in the `uv_coords` input below.
    ivec2 output_size;

    // Position (in output_size pixels) of last click.
    vec2 click_pos;
    // Position (in output_size pixels) of pointer position.
    vec2 hover_pos;
    // Position (in `output_size` pixels) where the mouse button was released.
    vec2 drag_target;

    // Velocity of the pointer averaged over the last few (3?) frames.
    vec2 pointer_velocity;

    // Arbitrary input data that can be set in the UI.
    vec4 input0;
    vec4 input1;
    vec4 input2;
    vec4 input3;
} pc;

// Every pipeline gets a linear & a nearest sampler.
layout(set=0, binding=1) uniform sampler nearestSampler;
layout(set=0, binding=2) uniform sampler linearSampler;



// Texture coords (0..1) of the rendered quad. This is usually entire texture or
// screen so it should be equivalent to `gl_FragCoord/pc.output_size`.
layout(location=0) in vec2 uv_coords;

// color output.
layout(location=0) out vec4 output_color;

// Texture inputs to shader are given from binding=3 and onward:
// layout(set=0, binding=3) uniform texture2D tex0;
// layout(set=0, binding=4) uniform texture2D tex1;
// etc.

const float PI  = 3.1415926535897932384626433832795;
const float TAU = 6.2831853071795864769252867665590;
const float PHI = 1.6180339887498948482045868343656; // Φ = Golden Ratio
