
struct VertexOutput {
    [[builtin(position)]] clip_position: vec4<f32>;
    [[location(0)]] uv: vec2<f32>;
};

[[stage(vertex)]]
fn main([[builtin(vertex_index)]] vertex_index: u32) -> VertexOutput {
    let y = ( f32(vertex_index & 1u)) * 2.0 - 1.0;
    let x = f32(abs(i32(vertex_index) - 3) & 2) - 1.0;

    var out: VertexOutput;
    out.clip_position = vec4<f32>(x, y, 0.5, 1.0);
    out.uv = vec2<f32>(1.0 + x, 1.0 - y) * 0.5;
    return out;
}
