

// Fast hashes.
//
// Most of these are based on the umix2() primitive. The hashX1() functions have
// been designed to be as random as practically possible while using only _a
// single 32-bit multiplication_.
//
// The basic uhash21() is about 1.8x faster on my 1080Ti than the integer hash
// iqint3() given by Inigo Quilez in https://www.shadertoy.com/view/4tXyWN with
// seemingly equivalent randomness characteristics.
//
// Note that most of the functions discard around 1.5 bits of entropy when
// converting the number from uint to float with a bitcast. This could be
// avoided if random floats in the range [1,2) were desired instead, but most
// algorithms work with signals in [0,1). See comments in uhash21() for more
// information.

// https://www.pcg-random.org/posts/does-it-beat-the-minimal-standard.html
const uint lcg_constant = 2739110765u;

// Randomly generated primes.
const uint prime1 = 2971768213u;
const uint prime2 = 3601995193u;
const uint prime3 = 1211974139u;
const uint prime4 = 1904838601u;

// Binary representation of 1.0
const uint float1_bits = 0x3f800000u;

// The basic non-linear mixing primitive.
//
// Its basic design goal is to produce output that always remains _visually
// random_ while using only a single multiplication.
//
// The most significant bits have the highest entropy in the output.
//
// The input was tested both as uvec2 integers and float-like bit patterns. The
// parameters were tuned practically, not derived mathematically. A number of
// inputs were given as both vec2 and uvec2, both as static and animated ranges.
// Output remained random for the most significant bits.
//
// The most significant bits were also used in converting the number back to
// floats, where they still remained "visually random" for the uniform
// distribution.

uint umix2(uvec2 u) {
    // The random primes are added to break up a linear relationship and make it
    // (at least) affine. Strictly not needed.
    u += uvec2(prime1, prime2);

    // At least four xor-shifts are necessary to give a cascade of 23+ bits (the
    // size of the float32 mantissa). The shift constants themselves are
    // somewhat arbitrary; other combinations might work. They were hand-picked
    // to give a reasonable cascade for both integer and float inputs.
    //
    // We're not iterating a "state" here so we don't need to worry about any
    // GF2 polynomials being irreducible.
    u ^= u.yx << 11u;
    u ^= u.yx >> 9u;
    u ^= u.yx << 3u;
    u ^= u.yx >> 1u;

    // Now for the multiplication operation.
    return u.x * u.y;
}


// The use of lcg() as an randomness extension function is _most likely_ bad.
//
// TODO: test this, find a better alternative.
//
uint lcg(uint v) {
    v *= lcg_constant;
    v += prime4;
    return v;
}

// Take this idea (from the PCG rng) and improve upon it; get away with a single
// mul?
uint pcg(uint v) {
    uint state = v * 747796405u + 2891336453u;
    uint word = ((state >> ((state >> 28u) + 4u)) ^ state) * 277803737u;
    return (word >> 22u) ^ word;
}


float uhash21(uvec2 u) {
    uint z = umix2(u);

    // This gives a random number in [1,2) with full ~23-bit pesudo-randomness.
    // Note we shift right instead of masking because the upper bits have the
    // highest entropy after a multiplication.
    float rand12 = uintBitsToFloat(float1_bits | (z >> 9));

    // Unfortunately we lose up to 1+1/2+1/4+...=2 bits in normalizing it to
    // [0,1) here, so the final result only has 21-22 bits of pseudo-randomness.
    return rand12 - 1.0;
}

float hash21(vec2 p) {
    return uhash21(floatBitsToUint(p));
}

vec2 uhash22(uvec2 u) {
    uint z1 = umix2(u);
    uint z2 = lcg(z1);
    uvec2 w = uvec2(z1, z2);
    return uintBitsToFloat(float1_bits | (w >> 9)) - 1.0;
}

vec2 hash22(vec2 p) {
    return uhash22(floatBitsToUint(p));
}

vec3 uhash23(uvec2 u) {
    uint z1 = umix2(u);
    uint z2 = lcg(z1);
    uint z3 = lcg(z2);
    uvec3 w = uvec3(z1, z2, z3);
    return uintBitsToFloat(float1_bits | (w >> 9)) - 1.0;
}

vec3 hash23(vec2 p) {
    return uhash23(floatBitsToUint(p));
}

vec4 uhash24(uvec2 u) {
    uint z1 = umix2(u);
    uint z2 = lcg(z1);
    uint z3 = lcg(z2);
    uint z4 = lcg(z3);
    uvec4 w = uvec4(z1, z2, z3, z4);
    return uintBitsToFloat(float1_bits | (w >> 9)) - 1.0;
}

vec4 hash24(vec2 p) {
    return uhash24(floatBitsToUint(p));
}


// TODO: further testing and commenting.
vec2 uhash32(uvec3 u) {
    u += uvec3(prime1, prime2, prime3);

    u ^= u.yzx >> 13u;
    u ^= u.yzx << 10u;
    u ^= u.yzx >> 6u;
    u ^= u.yzx << 3u;
    u ^= u.zyx >> 1u;

    uvec2 z = u.yx * u.zy;

    vec2 rand12 = uintBitsToFloat(0x3f800000u | (z >> 9));
    return rand12 - 1.0;
}

vec2 hash32(vec3 p) {
    return uhash32(floatBitsToUint(p));
}


#ifdef RNG_TEST

float extractBit(float v, uint bit) {
    uint u = floatBitsToUint(v);
    u >>= bit;
    return float(u & 1u);
}


const float border_width = 3.0;

void main_test_hash21() {
    vec2 pixel_size = 1.0 / pc.output_size;

    vec2 border = smoothstep(pixel_size*border_width,pixel_size*(border_width+1.5),2*abs(uv_coords-0.5));
    vec2 side = step(0.5, uv_coords);
    vec2 uv = mod(uv_coords, 0.5) * 2;

    uv += pc.input2.xy;
    uv *= pc.output_size * 2;
    uv *= pc.input2.zw;

    vec2 r1;
    vec2 r2;
    if (pc.input1.x>0.5) {
        r1 = vec2(uhash21(uvec2(uv)),uhash21(uvec2((uv - 0.5) * pc.input0.xy)));
        r2 = vec2(uhash21(uvec2(1048576+uv)),uhash21(uvec2(uv)*65536u));
    } else {
        r1 = vec2(hash21(uv),hash21((uv - 0.5) * pc.input0.xy));
        r2 = vec2(hash21(1048576+uv),hash21(uv*65536));
    }
    vec2 r3 = mix(r1, r2, side.x);
    float val = mix(r3.x, r3.y, side.y);

    if (pc.input1.z > 0.66) {
        val = extractBit(val, uint(pc.input1.w));
    }
    output_color = vec4(from_srgb(vec3(val)) * border.x * border.y, 1);
}



void main_test_hash32() {
    vec2 pixel_size = 1.0 / pc.output_size;

    vec2 border = smoothstep(pixel_size*border_width,pixel_size*(border_width+1.5),2*abs(uv_coords-0.5));
    vec2 side = step(0.5, uv_coords);
    vec3 uv = vec3(mod(uv_coords, 0.5) * 2, pc.input0.z);
    if (pc.input0.w > 0.75) {
        uv = uv.yzx;
    } else if (pc.input0.w > 0.5) {
        uv = uv.zxy;
    }


    uv += vec3(pc.input2.xy, 0);
    uv *= vec3(pc.output_size * 2, 1);
    uv *= vec3(pc.input2.zw, 1);

    vec2 q0,q1,q2,q3;
    if (pc.input1.x>0.5) {
        q0 = uhash32(uvec3(uv));
        q1 = uhash32(uvec3((uv - 0.5) * vec3(pc.input0.xy, 1)));
        q2 = uhash32(uvec3(1048576+uv));
        q3 = uhash32(uvec3(uv)*65536u);
    } else {
        q0 = hash32(uv);
        q1 = hash32((uv - 0.5) * vec3(pc.input0.xy, 1));
        q2 = hash32(1048576+uv);
        q3 = hash32(uv*65536);
    }
    vec2 c2 = mix(mix(q0, q1, side.x),mix(q2,q3,side.x),side.y);
    vec3 c = vec3(c2.x*c2.y, c2.x*(1-c2.y), c2.y*(1-c2.x));
    if (pc.input1.z > 0.66) {
        c = step(0.9,1-c);
    }
    output_color = vec4(from_srgb(c) * border.x * border.y, 1);
}

#endif
