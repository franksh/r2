

# about

Shader studio GUI.

Written in Rust. It uses [https://wgpu.rs](wgpu) for actually interacting with the GPU, Google's `Shaderc` for GLSL to SpirV compilation, and `egui` for the GUI layer.

Originally inspired by [https://shadertoy.com](Shadertoy).

# why

- Allows me to use an external editor! `r2` automatically detects file changes and hotloads the shaders.
- Native GPU usage instead of relying on the browser's GPU implementation.
- Allows better control of inputs like textures and uniforms.
  - For example, I can add GUI controls to control input parameters to better visualize adjustments.
- Rudimentary support for including files in GLSL to factor out rng-code et cetera.

# screenshots

![](screenshot1.png)

![](screenshot2.png)
