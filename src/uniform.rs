pub use crevice::std140::{AsStd140,Std140};

#[derive(Copy, Clone, AsStd140)]
pub struct Uniform {
  pub time: f32,
  pub delta: f32,
  pub frame: u32,
  pub aspect: f32,
  pub size: mint::Vector2<u32>,
  pub noise: mint::Vector4<f32>,
  pub mouse_pos: mint::Vector2<f32>,
  pub mouse_vel: mint::Vector2<f32>,
}

impl Uniform {
  pub fn zero() -> Self {
    Self {
      time: 0f32,
      delta: 0f32,
      frame: 0u32,
      aspect: 0f32,
      size: [0; 2].into(),
      noise: [0.0f32; 4].into(),
      mouse_pos: [0f32; 2].into(),
      mouse_vel: [0f32; 2].into(),
    }
  }
}
