//! Compilation running in a separate thread.
//!
//!

use std::{
  sync::{mpsc},
  thread, fs,
  time::Duration,
  path::*,
};
use log::error;
use notify::{
  RecommendedWatcher,
  DebouncedEvent,
  RecursiveMode,
  Watcher,
};
use anyhow::{
  Result,
};


pub struct FileWatch {
  file_watcher: RecommendedWatcher,
  events_tx: mpsc::Sender<DebouncedEvent>,
  join_handle: thread::JoinHandle<()>,
}


impl FileWatch {
  pub fn start<F>(mut process: F) -> Result<FileWatch> where
    F: 'static + Send + FnMut(&Path),
  {
    let (events_tx, events_rx) = mpsc::channel();
    let file_watcher = notify::watcher(events_tx.clone(), Duration::from_millis(300))?;

    let join_handle = thread::spawn(move || {
      while let Ok(evt) = events_rx.recv() {
        match evt {
          DebouncedEvent::Write(pth) => (process)(&pth),
          DebouncedEvent::Error(err, pth) => {
            error!("error hit with file {:?}: {:?}", pth, err);
            break;
          },
          _ => (),
        }
      }
    });

    Ok(Self {
      file_watcher,
      events_tx,
      join_handle,
    })
  }

  pub fn watch(&mut self, path: impl AsRef<Path>) -> Result<()> {
    let path = fs::canonicalize(path)?;
    self.file_watcher.watch(&path, RecursiveMode::NonRecursive)?;
    self.events_tx.send(DebouncedEvent::Write(path))?;
    Ok(())
  }

  pub fn unwatch(&mut self, path:impl AsRef<Path>) -> Result<()> {
    self.file_watcher.unwatch(path);
    Ok(())
  }

  pub fn join(self) -> Result<()> {
    match self.join_handle.join() {
      Ok(_) => Ok(()),
      Err(err) => std::panic::resume_unwind(err),
    }
  }
}


// fn watch_loop(target: Arc<Mutex<String>>, events: mpsc::Receiver<DebouncedEvent>) -> Result<()> {
//   loop {
//     let evt = events.recv()?;
//     match evt {
//       DebouncedEvent::Write(pth) => {
//         if let Ok(mut inner) = target.lock() {
//           *inner = fs::read_to_string(&pth)?;
//         } else {
//           anyhow::bail!("failed to acquire lock");
//         }
//       },
//       other => {
//         println!("noticed other: {:?}", other);
//       },
//     }
//   }
// }

// pub struct FileWatch {
//   pub data: Arc<Mutex<String>>,
//   file_watcher: RecommendedWatcher,
//   join_handle: thread::JoinHandle<Result<()>>,
// }

// impl FileWatch {
//   pub fn watch(filename: &str) -> Result<FileWatch> {
//     let (tx, rx) = mpsc::channel();
//     let mut file_watcher = watcher(tx, Duration::from_millis(300))?;
//     let data = Arc::new(Mutex::new(fs::read_to_string(str)));
//     file_watcher.watch(filename, RecursiveMode::Recursive)?;
//     let data2 = Arc::clone(&data);
//     let join_handle = thread::spawn(move || watch_loop(data2, rx));
//     Ok(FileWatch {
//       data, file_watcher, join_handle,
//     })
//   }

//   pub fn get_contents(&self) -> String {
//     let mut res = String::new();
//     if let Ok(x) = self.data.lock() {
//       res = x.clone();
//     }
//     res
//   }
// }
