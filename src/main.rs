
#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate derive_new;

use log::{info,trace,error,warn,debug};
// use futures_lite::future;
use std::sync::mpsc;
use std::{
  thread,
  path::Path,
  sync::{
    Arc,
    atomic::{Ordering, AtomicBool},
  },
};
use parking_lot::{Mutex};
use anyhow::{Result,Context};
use winit::{
  event::{Event,WindowEvent},
  event_loop::{EventLoop,ControlFlow},
};
use wgpu::{
  SurfaceTexture,
};
use egui_wgpu_backend::ScreenDescriptor;
use slotmap::{Key};


mod frame_time;
use frame_time::{WallTime,TickTime};

// mod arc_option;
mod gpu;

mod egui_state;
use egui_state::EguiState;

mod shady;
use shady::*;

// mod glsl;
mod file_watch;
mod include_file;

mod glsl_compiler;
mod ui;


static SHOULD_QUIT: AtomicBool = AtomicBool::new(false);



pub struct SubR2 {
  wall_time: WallTime,
  selected_shader: ShadyId,
  shady: ShadyManager,
  output_changed: bool,
}

pub struct R2 {
  gpu: Arc<gpu::Gpu>,
  // recv_err: mpsc::Receiver<wgpu::Error>,
  egui: Arc<Mutex<EguiState>>,

  // egui_ids: SecondaryMap<ShadyId,egui::TextureId>,
  r2ui: SubR2,
}


impl R2 {
  pub fn new(gpu: Arc<gpu::Gpu>, egui: Arc<Mutex<EguiState>>) -> Self {
    Self {
      gpu: gpu.clone(),
      // recv_err,
      egui,
      // egui_ids: slotmap::SecondaryMap::default(),
      r2ui: SubR2 {
        wall_time: WallTime::default(),
        selected_shader: ShadyId::null(),
        shady: ShadyManager::new(gpu),
        output_changed: false,
      },
    }
  }

  pub fn reset_shader(&mut self, path: impl AsRef<Path>, spirv_code: Vec<u32>) {
    let name = path.as_ref().file_stem().map_or(String::new(), |x| x.to_string_lossy().into());
    info!("resetting shader {} (file: {:?})", name, path.as_ref());
    let key = self.r2ui.set_code(name, spirv_code);

    {
      let mut egui = self.egui.lock();
      self.r2ui.shady.update_egui_id(key, &mut egui.render_pass);
    }
  }

  pub fn render(&mut self, encoder: &mut wgpu::CommandEncoder, frame: &SurfaceTexture, screen: &ScreenDescriptor) {
    self.r2ui.render(&self.gpu, encoder);

    if let mut egui = self.egui.lock() {
      if self.r2ui.output_changed {
        self.r2ui.shady.update_egui_id(self.r2ui.selected_shader, &mut egui.render_pass);
        self.r2ui.output_changed = false;
      }
      let c = self.r2ui.wall_time.current_time();
      let mesh = egui.assemble_ui(&mut self.r2ui, c);
      egui.render(&self.gpu, &screen, encoder, &frame.texture.create_view(&wgpu::TextureViewDescriptor::default()), mesh);
    } else {
      panic!("egui render lock failed");
    }
  }
}

impl SubR2 {
  pub fn render(&mut self, _gpu: &gpu::Gpu, encoder: &mut wgpu::CommandEncoder) {
    self.wall_time.advance();

    self.shady.render(encoder, &self.wall_time);
  }

  pub fn set_code(&mut self, name: String, spirv_code: Vec<u32>) -> ShadyId {
    let key = self.shady.get_or_create(name);

    self.shady.set_code(key, spirv_code);

    if self.selected_shader.is_null() {
      self.selected_shader = key;
    }

    key
  }
}

impl ui::UiFrame for SubR2 {
  fn ui_top(&mut self, ui: &mut egui::Ui) {
    self.shady.add_list_ui(&mut self.selected_shader, ui);
  }

  fn ui_left(&mut self, ui: &mut egui::Ui) {
    egui::CollapsingHeader::new("Wall Time").default_open(true).show(ui, |ui| {
      self.wall_time.add_ui(ui);
    });
    self.output_changed |= self.shady.add_ui(ui, self.selected_shader);
 }

  fn ui_central(&mut self, ui: &mut egui::Ui) {
    self.shady.add_central_ui(ui, self.selected_shader);
  }

}


#[derive(PartialEq,Eq,Debug,Copy,Clone)]
pub struct CustomEvent {

}



fn main_fallible() -> Result<()> {
  let event_loop = EventLoop::with_user_event();
  let window = winit::window::WindowBuilder::new()
    .with_decorations(true)
    .with_resizable(true)
    .with_transparent(false)
    .with_title("r2")
    .build(&event_loop)
    .context("window creation")?;

  let gpu = gpu::Gpu::create(&window).context("GPU initialization")?;
  let window_size = window.inner_size();
  let _window_scale = window.scale_factor();

  let gpu = Arc::new(gpu);

  let egui = egui_state::EguiState::create(&gpu, window_size, 1.0);
  let egui = Arc::new(Mutex::new(egui));

  let app = Arc::new(Mutex::new(R2::new(Arc::clone(&gpu), Arc::clone(&egui))));
  let _glsl_compiler = glsl_compiler::CompileThread::spawn(Arc::clone(&app))?;

  let (render_tx, render_rx) = mpsc::channel();
  let mut render_thread = Some(thread::spawn(move || {
    let mut surface = gpu.surface.lock();
    while !SHOULD_QUIT.load(Ordering::SeqCst) {
      if let Ok((sz, _scale)) = render_rx.try_recv() {
        surface.resize(&gpu.device, gpu.format, sz, 1.0);
      }
      let screen_descriptor = ScreenDescriptor {
        physical_width: surface.width,
        physical_height: surface.height,
        scale_factor: 1.0f32,
      };
      let frame = match surface.get_current_frame() {
        Ok(frame) => frame.output,
        Err(e) => {
          warn!("dropped a frame: {}", e);
          continue;
        },
      };
      let mut encoder = gpu.device.create_command_encoder(&wgpu::CommandEncoderDescriptor::default());

      {
        let mut app = app.lock();
        app.render(&mut encoder, &frame, &screen_descriptor);
      }
      gpu.queue.submit(std::iter::once(encoder.finish()));
      // Drop of swapchain frame leads to block?
    }
    info!("render exited thread");
  }));

  // thread::spawn(move || {
  //   while let Ok(err) = recv_err.recv() {
  //     SHOULD_QUIT.store(true, Ordering::SeqCst);
  //     println!("GPU ERROR: {:?}", err);
  //     error!("GPU ERROR: {:?}", err);
  //   }
  // });

  // let (events_tx, events_rx) = mpsc::channel();
  // let mut event_thread = Some(thread::spawn(move || {
  //   while let Ok(event) = events_rx.recv() {
  //     egui.handle_event(&event);
  //     if !egui.captures_event(&event) {

  //     }
  //   }
  // }));

  event_loop.run(move |event, _, control_flow| {
    let mut capture = false;
    {
      if let mut gui = egui.lock() {
        if gui.platform.captures_event(&event) {
          capture = true;
        }
        gui.platform.handle_event(&event);
      } else {
        panic!("egui event lock failed");
      }
    }
    debug!("event: {:?}", event);
    match event {
      Event::NewEvents(_start_cause) => (),
      Event::MainEventsCleared => {
        // window.request_redraw();
      },
      Event::RedrawRequested(..) => {
        // redraw_tx.send(());
        *control_flow = ControlFlow::Poll;
      },
      Event::RedrawEventsCleared => {
        *control_flow = ControlFlow::Wait;
      },
      Event::UserEvent(CustomEvent {  }) => {
        // window.request_redraw();
      },
      Event::WindowEvent { event, .. } => {
        match event {
          WindowEvent::Resized(size) => {
            render_tx.send((size, window.scale_factor()));
          },
          WindowEvent::ScaleFactorChanged { scale_factor, new_inner_size } => {
            render_tx.send((*new_inner_size, scale_factor));
          },
          WindowEvent::CloseRequested | WindowEvent::KeyboardInput {
            input: winit::event::KeyboardInput {
              state: winit::event::ElementState::Pressed,
              virtual_keycode: Some(winit::event::VirtualKeyCode::Escape), ..
            },
            ..
          } => {
            trace!("esc hit");
            *control_flow = ControlFlow::Exit;
          },
          _ => (),
          // winevt => if let Some(evt) = winevt.to_static() {
          //   // events_tx.send(evt);
          // },
        }
      },
      Event::DeviceEvent { .. } => (),
      Event::Suspended => info!("suspended event"),
      Event::Resumed => info!("resumed event"),
      Event::LoopDestroyed => {
        info!("main loop shutting down");
        SHOULD_QUIT.store(true, Ordering::SeqCst);
        render_thread.take().map(thread::JoinHandle::join);
      },
    }
    if SHOULD_QUIT.load(Ordering::SeqCst) {
      *control_flow = ControlFlow::Exit;
    }
    trace!("control flow = {:?}", control_flow);
  });
}


fn main() {
  env_logger::init();

  trace!("application starting");

  match main_fallible() {
    Ok(()) => warn!("impossible exit"),
    Err(err) => {
      error!("main quit with error: {:?}", err);
    }
  }
}
