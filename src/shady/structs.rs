use wgpu::*;

pub fn extent3d(size: [u32; 2]) -> Extent3d {
  Extent3d {
    width: size[0],
    height: size[1],
    depth_or_array_layers: 1,
  }
}

pub fn sampler(address_mode: AddressMode) -> SamplerDescriptor<'static> {
  SamplerDescriptor {
    address_mode_u: address_mode,
    address_mode_v: address_mode,
    address_mode_w: address_mode,
    mag_filter: wgpu::FilterMode::Linear,
    min_filter: wgpu::FilterMode::Nearest,
    mipmap_filter: wgpu::FilterMode::Nearest,
    ..Default::default()
  }
}

pub fn layout_entry(entry: u32, ty: BindingType) -> BindGroupLayoutEntry {
  BindGroupLayoutEntry {
    binding: entry,
    visibility: ShaderStages::FRAGMENT,
    count: None,
    ty: ty,
  }
}
