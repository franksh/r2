//! Simple file inclusion system.
//!
//! This module provides the struct [`IncludeFile`] which contains the contents
//! as well as a set of dependencies for a given file.
//!
//! All filenames are canonicalized with `fs::std::canonicalize`.

use anyhow::{Context,Result};
use std::{
  fs,
  path::*,
  collections::{HashSet},
};

#[derive(Default,Clone,Eq,PartialEq,Debug)]
pub struct IncludeFile {
  pub filename: PathBuf,
  pub source: String,
  pub includes: HashSet<PathBuf>,
}


impl IncludeFile {
  pub fn build(include_pattern: &regex::Regex, filename: impl AsRef<Path>) -> Result<Self> {
    let mut res = Self::default();
    res.filename = fs::canonicalize(filename.as_ref())?;
    res.append_include(&include_pattern, filename)?;
    Ok(res)
  }

  pub fn full_path(&self) -> &Path {
    &self.filename
  }

  pub fn basename(&self) -> String {
    self.filename.with_extension("").file_name().unwrap().to_string_lossy().into()
  }

  pub fn includes(&self, path: impl AsRef<Path>) -> bool {
    self.includes.contains(path.as_ref())
  }

  pub fn include_iter(&self) -> impl Iterator<Item=&PathBuf> {
    self.includes.iter()
  }

  fn append_include(&mut self, include_pattern: &regex::Regex, filename: impl AsRef<Path>) -> Result<()> {
    let filename = fs::canonicalize(filename)?;
    let src = fs::read_to_string(&filename).context("failed to read file")?;
    self.source.reserve(src.len());
    let root = filename.parent().unwrap_or(Path::new("."));
    let mut src_pos = 0;

    for cap in include_pattern.captures_iter(&src) {
      let tm = cap.get(0).unwrap();
      self.source.push_str(&src[src_pos..tm.start()]);
      src_pos = tm.end();

      let fm = cap.get(1).unwrap();
      self.append_include(&include_pattern, &root.join(fm.as_str())).context("file inclusion")?;
    }
    self.source.push_str(&src[src_pos..src.len()]);
    self.includes.insert(filename);
    Ok(())
  }
}




#[cfg(test)]
mod test {
  use super::*;

  #[test]
  pub fn test_regex() {
    let inc = IncludeSet::new();
    let re = inc.regex;
    let mut it = re.captures_iter("..//include:a/b/c//..//include: d/e/f //..");
    let cap1 = it.next().unwrap();
    let m1f = cap1.get(1).unwrap();
    let m1t = cap1.get(0).unwrap();
    assert_eq!(m1f.as_str(), "a/b/c");
    assert_eq!(m1t.range(), 2..19);
    let cap2 = it.next().unwrap();
    let m2 = cap2.get(1).unwrap();
    assert_eq!(m2.as_str(), "d/e/f");
  }

  #[test]
  pub fn test_files() {
    let inc = IncludeSet::new();
    let fil = inc.build_include_file("test/root1.test");
    let res = "R1
Child1 {
//
// [[Child2{
((CHILD3
))
}/Child2
]] //
// [[CHILD3
]] //
//
} Child1

R2
Child2{
((CHILD3
))
}/Child2

R3
CHILD3

R4
";
    assert_eq!(fil.source, res);
  }

  #[test]
  #[should_panic]
  fn test_fnf() {
    let inc = IncludeSet::new();
    let fil = inc.build_include_file("test/root_nonexistant.test");
  }
}
