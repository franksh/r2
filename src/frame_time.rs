//! Utility types for tracking time, frames, and ticks.
//!
use egui::util::History;
use std::time::{Instant,Duration};

/// Real (wall) time tracking.
#[derive(Clone)]
pub struct WallTime {
  pub start: Instant, // Time of application start.
  pub current: Duration, // Current timestamp relative to start.
  pub delta: Option<f64>, // The delta of last time step.
  history: History<f64>, // History of frame deltas.
}

impl Default for WallTime {
  fn default() -> Self {
    Self {
      start: Instant::now(),
      current: Duration::default(),
      delta: None,
      history: History::new(61, 180.0),
    }
  }
}

impl WallTime {
  pub fn advance(&mut self) {
    let new_current = self.start.elapsed();
    let delta = (new_current - self.current).as_secs_f64();

    self.current = new_current;
    self.delta = Some(delta);
    self.history.add(self.current_time(), delta);
  }

  pub fn current_time(&self) -> f64 {
    self.current.as_secs_f64()
  }

  pub fn fps(&self) -> f32 {
    60.0 / self.history.duration()
  }
}

impl WallTime {
  pub fn add_ui(&mut self, ui: &mut egui::Ui) {
    ui.label("Wall time statistics. This data is not fed to shaders.");
    ui.label(format!("Frames per second: {:.2}ms", self.fps()));
    ui.label(format!("Frame delta: {:.2}ms", 1000.0 * self.delta.unwrap_or_default()));
    ui.label(format!("Timestamp {:.1}s", self.current.as_secs_f32()));
  }
}

/// Tracking time and ticks in a fixed ticks-per-second timeline.
#[derive(Clone,PartialEq)]
pub struct TickTime {
  tick_time: f64, // Seconds per tick (inverse of ticks per second).
  pub time: f64, // The current timestamp.
  pub tick: isize,
  time_scale: f64,
  paused: bool,
}

impl Default for TickTime {
  fn default() -> Self {
    Self {
      tick_time: 1.0f64/60.0,
      time: 0.0,
      tick: 0,
      time_scale: 1.0,
      paused: false,
    }
  }
}

impl TickTime {
  pub fn from_tick_time(tick_time: f64) -> Self {
    Self {
      tick_time,
      ..Default::default()
    }
  }

  pub fn reset(&mut self) {
    self.time = 0.0;
    self.tick = 0;
  }

  pub fn advance(&mut self) {
    if self.is_paused() {
      return;
    }
    self.advance_ticks(1);
  }

  pub fn advance_ticks(&mut self, ticks: isize) {
    self.tick += ticks;
    if self.tick < 0 {
      self.reset();
    } else {
      self.time += self.real_tick_time() * (ticks as f64);
    }
  }

  pub fn real_tick_time(&self) -> f64 {
    self.tick_time * self.time_scale
  }

  pub fn is_paused(&self) -> bool {
    self.paused
  }
}

impl TickTime {
  pub fn add_ui(&mut self, ui: &mut egui::Ui) {
    ui.label("Timeline data fed to shaders. This is fixed by ticks and independent of the wall clock.");
    ui.horizontal(|ui| {
      ui.label(format!("Tick: {}", self.tick));
      if ui.button("⏮").clicked() {
        self.reset();
      }
      if ui.button("-1").clicked() {
        self.advance_ticks(-1);
      }
      if ui.button("+1").clicked() {
        self.advance_ticks(1);
      }
    });
    ui.horizontal(|ui| {
      ui.label(format!("Timestamp: {:.2}s", self.time));
      if ui.button("⏮").clicked() {
        self.reset();
      }
      if ui.button("-1").clicked() {
        self.advance_ticks( (-1.0/self.real_tick_time()).round() as isize );
      }
      if ui.button("+1").clicked() {
        self.advance_ticks( (1.0/self.real_tick_time()).round() as isize );
      }
    });
    ui.label(format!("Tick time: {:.3} ({:.2} tps)", self.tick_time, 1.0/self.tick_time));
    ui.horizontal(|ui| {
      ui.label(format!("Time scale playback {:.3}", self.time_scale));
      if self.paused {
        ui.label("(paused)");
      }
      if ui.button(if self.is_paused() {"▶"} else {"⏸"}).clicked() {
        self.paused = !self.paused;
      }
      if ui.button("⇄").clicked() {
        self.time_scale *= -1.0;
      }
      if ui.button("⏪").clicked() {
        self.time_scale *= 0.8;
      }
      if ui.button("⏩").clicked() {
        self.time_scale *= 1.25;
      }
    });
  }
}
