
use egui_winit_platform::{
  Platform,
  PlatformDescriptor,
};
use egui_wgpu_backend::{
  RenderPass,
  ScreenDescriptor,
};

use super::gpu::{Gpu};
use super::ui::UiFrame;


pub struct EguiState {
  pub platform: Platform,
  pub render_pass: RenderPass,
}

impl EguiState {
  pub fn create(gpu: &Gpu, size: winit::dpi::PhysicalSize<u32>, scale: f64) -> Self {
    let fonts = egui::FontDefinitions::default();

    let platform = Platform::new(PlatformDescriptor {
      physical_width: size.width,
      physical_height: size.height,
      scale_factor: scale,
      font_definitions: fonts,
      style: Default::default(),
    });
    let render_pass = RenderPass::new(&gpu.device, gpu.format, 1);

    Self {
      platform,
      render_pass,
    }
  }

  pub fn assemble_ui(&mut self, app: &mut impl UiFrame, time: f64) -> Vec<egui::ClippedMesh> {
    self.platform.update_time(time);
    self.platform.begin_frame();

    let ctx = self.platform.context();
    egui::TopBottomPanel::top("top-down-panel").show(&ctx, |ui| {
      ui.horizontal_wrapped(|ui| {
        ui.label("R2");
        ui.separator();
        app.ui_top(ui);
        ui.with_layout(egui::Layout::right_to_left(), |ui| {
          egui::warn_if_debug_build(ui);
        });
      });
    });

    egui::SidePanel::left("left-panel")
      .resizable(true).show(&ctx, |ui| {
        app.ui_left(ui);
      });
    egui::CentralPanel::default().show(&ctx, |ui| app.ui_central(ui));

    let (output, paint_commands) = self.platform.end_frame(None); // NOTE: provide window.

    ctx.tessellate(paint_commands)
  }

  pub fn context(&self) -> egui::CtxRef {
    self.platform.context()
  }

  pub fn render(&mut self, gpu: &Gpu, screen: &ScreenDescriptor, encoder: &mut wgpu::CommandEncoder, output_frame: &wgpu::TextureView, mesh: Vec<egui::ClippedMesh>) {
    let ctx = self.context();

    self.render_pass.update_texture(&gpu.device, &gpu.queue, &ctx.texture());
    self.render_pass.update_user_textures(&gpu.device, &gpu.queue);
    self.render_pass.update_buffers(&gpu.device, &gpu.queue, &mesh, &screen);

    self.render_pass.execute(
      encoder,
      &output_frame,
      &mesh,
      &screen,
      Some(wgpu::Color::BLACK),
    );
  }

}
