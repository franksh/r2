
use wgpu::{
  Texture, TextureFormat,
};
use std::sync::Arc;
use crate::include_file::IncludeFile;
use super::ui::UiFrame;


pub struct ShaderManager {
  shaders: Vec<Shader>,
}

pub TextureInfo {
  name: String,
  size: [u32; 2],
  format: TextureFormat,
  texture: Texture,
}

impl TextureInfo {
  pub fn create(basename: impl AsRef<str>) -> Self {
    let name = format!("{}:{}x{}:{:?}", basename, size[0], size[1], format);
  }
}

#[derive(Clone,Debug)]
pub struct Shader {
  manager: &Manager,
  file: IncludeFile,
  // shady: Shady,
  inputs: Vec<Arc<Shader>>,
  output: TextureInfo,
}

impl Shader {
  pub fn attach_input() {

  }

  pub fn name(&self) -> &str {
    self.file.basename()
  }

  pub new_output(&mut self) {
    format!(self.name()
  }
}

impl Shader {
  fn ui_texture(&mut self, ui: &mut egui::Ui) {
    egui::ComboBox::from_label("show")
      .selected_text("XXX")
      .show_ui(ui, |ui| {

        for (i,tex) in self.0 {
        ui.selectable_value(&mut v, 0, "tex1");
      });
  }
}


impl UiFrame for Shader {
  fn ui_left(&mut self, ui: &mut egui::Ui) {
    ui.label(format!("name: {}", self.name()));
    ui.label("inputs");
    for ix in self.inputs.iter() {

    }
    if ui.button("add").clicked() {

    }

    ui.label("outputs");
    for ix in self.outputs.iter() {

    }
    if ui.button("add").clicked() {

    }
  }
}
