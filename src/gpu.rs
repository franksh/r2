use parking_lot::{Mutex};
use futures_lite::future;
use log::{info};
use anyhow::{Result,Context};
use wgpu::{
  BufferUsages,
  util::{
    DeviceExt,
    BufferInitDescriptor,
  },
};

pub struct Surface {
  pub width: u32,
  pub height: u32,
  pub scale: f64,
  surface: wgpu::Surface,
}

impl std::ops::Deref for Surface {
  type Target = wgpu::Surface;
  fn deref(&self) -> &Self::Target {
    &self.surface
  }
}

// impl Surface {
//   pub fn new(surface: wgpu::Surface) -> Self {
//     Self {
//       width: 0,
//       height: 0,
//       scale: 0.0,
//       surface,
//     }
//   }
// }

pub struct Gpu {
  pub device: wgpu::Device,
  pub queue: wgpu::Queue,
  pub format: wgpu::TextureFormat,
  pub surface: Mutex<Surface>,
}


impl Surface {
  pub fn resize(&mut self, device: &wgpu::Device, format: wgpu::TextureFormat, size: winit::dpi::PhysicalSize<u32>, scale: f64) {
    self.width = size.width;
    self.height = size.height;
    self.scale = scale;
    self.configure(&device, &wgpu::SurfaceConfiguration {
      usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
      format,
      width: size.width,
      height: size.height,
      present_mode: wgpu::PresentMode::Fifo,
    });
  }
}

impl Gpu {
  pub fn create(window: &winit::window::Window) -> Result<Gpu> {
    let instance = wgpu::Instance::new(wgpu::Backends::PRIMARY);
    let surface = unsafe { instance.create_surface(window) };

    let adapter = future::block_on(
      instance.request_adapter(&wgpu::RequestAdapterOptions {
        power_preference: wgpu::PowerPreference::HighPerformance, // default(), // PowerPreference::LowPower,
        compatible_surface: Some(&surface),
      })
    ).context("wgpu adapter")?;

    let format = surface.get_preferred_format(&adapter).unwrap();

    let adapter_info = adapter.get_info();
    info!("Adapter: {}: {:?} ({:?})", adapter_info.name, adapter_info.device_type, adapter_info.backend);

    let (device, queue) = future::block_on(
      adapter.request_device(
        &wgpu::DeviceDescriptor {
          label: None,
          features: wgpu::Features::PUSH_CONSTANTS,
          limits: wgpu::Limits {
            max_push_constant_size: 128,
            ..wgpu::Limits::default()
          },
        },
        None, // trace path.
      )
    ).context("wgpu device")?;

    let window_size = window.inner_size();
    let window_scale = window.scale_factor();
    let mut surface = Surface {
      width: window_size.width,
      height: window_size.height,
      scale: window_scale,
      surface,
    };

    surface.resize(&device, format, window_size, window_scale);

    Ok(Gpu {
      device,
      queue,
      format,
      surface: Mutex::new(surface),
    })
  }

  pub fn make_buffer(&self, data: &[u8], usage: BufferUsages) -> wgpu::Buffer {
    self.device.create_buffer_init(&BufferInitDescriptor {
      label: None,
      contents: data,
      usage,
    })
  }

  pub fn texture2d(&self, size: [u32; 2], format: wgpu::TextureFormat, usage: wgpu::TextureUsages) -> wgpu::Texture {
    self.device.create_texture(&wgpu::TextureDescriptor {
      label: None,
      format,
      usage,
      size: crate::shady::structs::extent3d(size),
      mip_level_count: 1,
      sample_count: 1,
      dimension: wgpu::TextureDimension::D2,
    })
  }
}
