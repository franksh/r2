
use std::{
  sync::Arc,
  path::*,
};
use parking_lot::Mutex;
use anyhow::{Result,Context};
use shaderc;
use log::{info,warn,debug};
use crate::file_watch::FileWatch;
use crate::include_file::IncludeFile;
use crate::R2;




pub fn compile_glsl(source: &str) -> Result<Vec<u32>> {
  let mut compiler = shaderc::Compiler::new().context("compiler instance")?;
  let options = shaderc::CompileOptions::new().context("compiler options")?;

  let binary = compiler.compile_into_spirv(
    source, shaderc::ShaderKind::Fragment,
    "default.glsl", "main", Some(&options)
  )?;

  Ok(binary.as_binary().to_owned())
}


lazy_static! {
  static ref INCLUDE_PATTERN: regex::Regex = regex::Regex::new(r"//include:\s*((?:[^/]|/[^/\s])+?)\s*//").unwrap();
}

pub struct CompileThread {
  pub r2: Arc<Mutex<R2>>,
  pub shaders: Vec<IncludeFile>,
}

impl CompileThread {
  pub fn spawn(r2: Arc<Mutex<R2>>) -> Result<FileWatch> {
    let shaders = std::env::args()
      .skip(1)
      .map(|path| IncludeFile::build(&INCLUDE_PATTERN, path).unwrap())
      .collect::<Vec<IncludeFile>>();

    info!("watching {} files", shaders.len());
    debug!("include files: {:?}", shaders);
    let mut th = CompileThread {
      r2,
      shaders,
    };

    let mut fw = FileWatch::start(move |pth| th.run(pth))?;

    for arg in std::env::args().skip(1) {
      info!("watching file {}", arg);
      fw.watch(arg)?;
    }

    Ok(fw)
  }

  pub fn run(&mut self, path: &Path) {
    let mut found = false;
    info!("received event for file {:?}", path);
    for fil in self.shaders.iter_mut() {
      if !fil.includes(&path) {
        continue;
      }
      info!("... affects {:?}", &fil.filename);

      *fil = match IncludeFile::build(&INCLUDE_PATTERN, path) {
        Ok(val) => val,
        Err(_) => continue,
      };

      // if let mut fw = self.file_watcher.lock() {
      //   for inc in fil.include_iter() {
      //     info!("would like to add {:?}", inc);
      //     // fw.watch(inc);
      //   }
      // }

      found = true;

      let data = match compile_glsl(&fil.source) {
        Ok(data) => data,
        Err(err) => {
          warn!("failed to compile: {:?}", err);
          continue;
        }
      };

      {
        info!("... resetting shader");
        let mut r2 = self.r2.lock();
        r2.reset_shader(fil.basename(), data);
      }
    }
    if !found {
      warn!("... affects no shaders!?");
      //   if let mut fw = self.file_watcher.lock() {
      //     info!("unwatching file {:?}", path);
      //     fw.unwatch(&path);
      //   }
    }
  }
}

