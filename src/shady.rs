
use std::{collections::{HashMap, HashSet}, sync::{Arc, mpsc::{self, Receiver}}};
use log::{error, info};
use ranges::GenericRange;
use slotmap::{SlotMap,SecondaryMap,new_key_type,Key};
use wgpu::{BindGroup, BindGroupEntry, BindingResource, BindingType, BlendState, BufferBindingType, BufferUsages, ColorWrites, CommandEncoder, LoadOp, PushConstantRange, RenderPipeline, ShaderSource, ShaderStages, TextureFormat, TextureSampleType, TextureUsages, TextureView, TextureViewDescriptor, TextureViewDimension};
use crevice::std140::{AsStd140,Std140};
use bytemuck::{bytes_of, Zeroable};

use crate::{frame_time::{TickTime, WallTime}, gpu::Gpu, ui::int_input::IntInput};

pub mod structs;


new_key_type! {
  pub struct ShadyId;
}

macro_rules! PASSTHROUGH_PATH {
  () => { "../res/passthrough.wgsl" }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum InputType { Color, Angle, Small }

impl InputType {
  pub fn as_range(self) -> std::ops::RangeInclusive<f32> {
    use InputType::*;
    match self {
      Color => 0.0..=1.0,
      Angle => 0.0..=std::f32::consts::TAU,
      Small => -10.0..=10.0,
    }
  }
}

fn input_ui(ui: &mut egui::Ui, typ: &mut InputType, input: &mut [f32;4]) {
  ui.horizontal(|ui| {
    ui.radio_value(typ, InputType::Color, "color (0..1)");
    ui.radio_value(typ, InputType::Angle, "angles (0..2pi)");
    ui.radio_value(typ, InputType::Small, "arbitrary");
  });

  match *typ {
    InputType::Angle => {
      egui::Grid::new("angle_g")
        .num_columns(2)
        .striped(true)
        .show(ui, |ui| {
          ui.label("x");
          ui.drag_angle(&mut input[0]);
          ui.end_row(); // TODO: test if needed
          ui.label("y");
          ui.drag_angle(&mut input[1]);
          ui.end_row();
          ui.label("z");
          ui.drag_angle(&mut input[2]);
          ui.end_row();
          ui.label("w");
          ui.drag_angle(&mut input[3]);
          ui.end_row();
        });
    },
    InputType::Color => {
      ui.add(egui::Slider::new(&mut input[0], 0.0..=1.0).text("x"));
      ui.add(egui::Slider::new(&mut input[1], 0.0..=1.0).text("y"));
      ui.add(egui::Slider::new(&mut input[2], 0.0..=1.0).text("z"));
      ui.add(egui::Slider::new(&mut input[3], 0.0..=1.0).text("w"));

      // The color button modifies input in the HSV conversion so need to give it a temporary.
      let mut tmp = *input;
      if ui.color_edit_button_rgba_unmultiplied(&mut tmp).changed() {
        *input = tmp;
      }
    },
    InputType::Small => {
      egui::Grid::new("small_g")
        .num_columns(2)
        .striped(true)
        .show(ui, |ui| {
          ui.label("x");
          ui.add(egui::DragValue::new(&mut input[0]).min_decimals(2).speed(0.01));
          ui.end_row();
          ui.label("y");
          ui.add(egui::DragValue::new(&mut input[1]).min_decimals(2).speed(0.01));
          ui.end_row();
          ui.label("z");
          ui.add(egui::DragValue::new(&mut input[2]).min_decimals(2).speed(0.01));
          ui.end_row();
          ui.label("w");
          ui.add(egui::DragValue::new(&mut input[3]).min_decimals(2).speed(0.01));
          ui.end_row();
        });
    },
  }
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Zeroable, bytemuck::Pod)]
pub struct PushValues {
  time: f32,
  tick: i32,
  output_size: [u32;2],

  click_pos: [f32;2],
  hover_pos: [f32;2],
  drag_target: [f32;2],
  pointer_velocity: [f32;2],

  input: [[f32;4];4],
}

pub struct Shady {
  name: String,
  size: [u32;2],
  format: TextureFormat,
  output: wgpu::Texture,
  inputs: HashSet<ShadyId>,
  view: TextureView,
  tick_time: TickTime,
  last_rendered: Option<isize>,

  input_type: [InputType;4],
  push_constants: PushValues,

  ui_input_width: IntInput<u32>,
  ui_input_height: IntInput<u32>,

  opt_scale_image: bool,
}

pub struct ShadyManager {
  gpu: Arc<Gpu>,
  recv_err: Receiver<wgpu::Error>,

  // uniform: Uniform,
  uniform_buffer: wgpu::Buffer,

  default_size: [u32; 2],
  default_format: TextureFormat,

  vertex_shader: wgpu::ShaderModule,
  linear_sampler: wgpu::Sampler,
  nearest_sampler: wgpu::Sampler,

  by_name: HashMap<String,ShadyId>,
  names: SlotMap<ShadyId,Shady>,

  shaders: SecondaryMap<ShadyId,wgpu::ShaderModule>,
  pipelines: SecondaryMap<ShadyId,Result<(RenderPipeline,BindGroup),String>>,
  egui_ids: SecondaryMap<ShadyId,egui::TextureId>,
}

impl ShadyManager {

  pub fn new(gpu: Arc<Gpu>) -> Self {
    let default_size = [1280u32, 768];
    let default_format = wgpu::TextureFormat::Rgba8UnormSrgb;

    // NOTE: find a use for uniform buffer?
    let uniform_buffer = gpu.make_buffer(&[0,0,0,0],
                                 BufferUsages::UNIFORM | BufferUsages::COPY_DST);

    let linear_sampler = gpu.device.create_sampler(&wgpu::SamplerDescriptor {
      address_mode_u: wgpu::AddressMode::MirrorRepeat,
      address_mode_v: wgpu::AddressMode::MirrorRepeat,
      address_mode_w: wgpu::AddressMode::MirrorRepeat,
      mag_filter: wgpu::FilterMode::Linear,
      min_filter: wgpu::FilterMode::Linear,
      mipmap_filter: wgpu::FilterMode::Linear,
      ..Default::default()
    });
    let nearest_sampler = gpu.device.create_sampler(&wgpu::SamplerDescriptor {
      address_mode_u: wgpu::AddressMode::MirrorRepeat,
      address_mode_v: wgpu::AddressMode::MirrorRepeat,
      address_mode_w: wgpu::AddressMode::MirrorRepeat,
      mag_filter: wgpu::FilterMode::Nearest,
      min_filter: wgpu::FilterMode::Nearest,
      mipmap_filter: wgpu::FilterMode::Nearest,
      ..Default::default()
    });
    let vertex_shader = gpu.device.create_shader_module(&wgpu::ShaderModuleDescriptor {
      label: None,
      source: ShaderSource::Wgsl(include_str!(PASSTHROUGH_PATH!()).into()),
    });

    // NOTE: there has to be a better way of doing errors?
    let (send_err, recv_err) = mpsc::channel();
    gpu.device.on_uncaptured_error(move |err| {
      error!("{:#?}", err);
      send_err.send(err);
    });


    Self {
      gpu,
      recv_err,

      uniform_buffer,
      default_size,
      default_format,

      vertex_shader,
      linear_sampler,
      nearest_sampler,

      by_name: HashMap::default(),
      names: SlotMap::default(),
      shaders: SecondaryMap::default(),
      pipelines: SecondaryMap::default(),
      // outputs: SecondaryMap::default(),
      // inputs: SecondaryMap::default(),
      egui_ids: SecondaryMap::default(),
    }
  }

  pub fn get_or_create(&mut self, name: impl AsRef<str>) -> ShadyId {
    let Self { gpu, by_name, names, default_size, default_format, pipelines, .. } = self;
    *by_name.entry(name.as_ref().to_owned()).or_insert_with(|| {
      let shady = Shady::new(&gpu, name.as_ref(), *default_size, *default_format);
      let key = names.insert(shady);
      pipelines.insert(key, Err("no shader set yet".to_string()));
      key
    })
  }

  pub fn update_egui_id(&mut self, key: ShadyId, backend: &mut egui_wgpu_backend::RenderPass) {
    if let Some(egui_id) = self.egui_ids.get(key) {
      backend.update_egui_texture_from_wgpu_texture(
        &self.gpu.device,
        self.texture(key),
        wgpu::FilterMode::Linear,
        *egui_id
      );
    } else {
      self.egui_ids.insert(key, backend.egui_texture_from_wgpu_texture(
        &self.gpu.device,
        self.texture(key),
        wgpu::FilterMode::Linear
      ));
    }
  }

  pub fn set_code(&mut self, key: ShadyId, code: Vec<u32>) {
    let shader = self.gpu.device.create_shader_module(&wgpu::ShaderModuleDescriptor {
      label: None,
      source: wgpu::ShaderSource::SpirV(code.into()),
    });
    self.shaders.insert(key, shader);
    self.build_pipeline(key);
  }

  pub fn set_output(&mut self, key: ShadyId, size: [u32;2], format: TextureFormat) {
    info!("want to reset size: {:?}, format: {:?}", size, format);
    self.names[key].set_output(&self.gpu, size, format);

    let mut to_reset = vec![];
    for (k,sh) in self.names.iter() {
      if sh.inputs.contains(&key) {
        to_reset.push(k);
      }
    }
    for k in to_reset {
      info!("Resetting {}", self.names[k].name);
      self.build_pipeline(k);
    }
  }

  pub fn texture(&self, key: ShadyId) -> &wgpu::Texture {
    &self.names[key].output
  }

  pub fn render_shader(&mut self, encoder: &mut CommandEncoder, key: ShadyId) {
    let (ref pipeline, ref binds) = match &self.pipelines[key] {
      Err(_) => return,
      Ok(v) => v,
    };

    self.names[key].render(encoder, pipeline, binds);
  }

  pub fn render(&mut self, encoder: &mut CommandEncoder, wall_time: &WallTime) {
    // self.gpu.queue.write_buffer(&self.uniform_buffer, 0, ...);

    let keys: Vec<_> = self.names.keys().collect();
    for k in keys {
      self.render_shader(encoder, k);
    }
  }

  pub fn build_pipeline(&mut self, key: ShadyId) {
    if !self.shaders.contains_key(key) {
      return;
    }
    let fragment_shader = &self.shaders[key];

    let mut layout_entries = vec![
      structs::layout_entry(0, BindingType::Buffer {
        ty: BufferBindingType::Uniform,
        has_dynamic_offset: false,
        min_binding_size: None, // XXX: ?
      }),
      structs::layout_entry(1, BindingType::Sampler {
        comparison: false,
        filtering: false, // XXX: ?
      }),
      structs::layout_entry(2, BindingType::Sampler {
        comparison: false,
        filtering: true, // XXX: ?
      }),
    ];

    let mut bind_entries = vec![
      BindGroupEntry {binding: 0, resource: self.uniform_buffer.as_entire_binding(),},
      BindGroupEntry {binding: 1, resource: BindingResource::Sampler(&self.nearest_sampler),},
      BindGroupEntry {binding: 2, resource: BindingResource::Sampler(&self.linear_sampler),},
    ];

    let views = self.names[key].inputs.iter().map(|k| self.texture(*k).create_view(&TextureViewDescriptor::default())).collect::<Vec<_>>();

    for (i,tv) in views.iter().enumerate() {
      layout_entries.push(structs::layout_entry(3+i as u32, BindingType::Texture {
        multisampled: false,
        view_dimension: TextureViewDimension::D2,
        sample_type: TextureSampleType::Float { filterable: true },
      }));
      bind_entries.push(BindGroupEntry {binding: 3+i as u32, resource: BindingResource::TextureView(&tv),});
    }

    let layout = self.gpu.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
      label: None,
      entries: &layout_entries,
    });
    let bind_group = self.gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
      layout: &layout,
      entries: &bind_entries,
      label: None,
    });
    let pipeline_layout = self.gpu.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
      label: None,
      bind_group_layouts: &[&layout],
      push_constant_ranges: &[PushConstantRange {
        stages: ShaderStages::FRAGMENT,
        range: 0..128,
      }],
    });

    let pipeline = self.gpu.device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
      label: None,
      layout: Some(&pipeline_layout),
      vertex: wgpu::VertexState {
        module: &self.vertex_shader,
        entry_point: "main",
        buffers: &[],
      },
      primitive: wgpu::PrimitiveState {
        topology: wgpu::PrimitiveTopology::default(),
        strip_index_format: None,
        front_face: wgpu::FrontFace::Ccw,
        cull_mode: None, // XXX: Some(Face::Back),
        polygon_mode: wgpu::PolygonMode::Fill,
        clamp_depth: false,
        conservative: false,
      },
      depth_stencil: None,
      multisample: wgpu::MultisampleState::default(),
      fragment: Some(wgpu::FragmentState {
        module: fragment_shader,
        entry_point: "main",
        targets: &[wgpu::ColorTargetState {
          format: self.names[key].format,
          blend: Some(BlendState::REPLACE),
          write_mask: ColorWrites::ALL,
        }],
      }),
    });

    if let Some(err) = self.any_errors() {
      self.pipelines[key] = Err(err);
    } else {
      self.pipelines[key] = Ok((pipeline, bind_group));
    }
  }

  fn any_errors(&self) -> Option<String> {
    let mut res = None;
    while let Ok(err) = self.recv_err.try_recv() {
      error!("GPU ERROR: {:?}", err);
      res = Some(format!("{}Error: {}", res.map(|s| s + "\n---\n").unwrap_or(String::new()), err));
    }
    res
  }
}

impl ShadyManager {
  pub fn add_ui(&mut self, ui: &mut egui::Ui, selected: ShadyId) -> bool {
    if selected.is_null() {
      return false;
    }
    egui::CollapsingHeader::new("Tick Time").default_open(true).show(ui, |ui| {
      self.names[selected].tick_time.add_ui(ui);
    });
    egui::CollapsingHeader::new("Input Values").default_open(true).show(ui, |ui| {
      let shady = &mut self.names[selected];
      shady.add_input_ui(ui);
    });
    egui::CollapsingHeader::new("Input Textures").default_open(true).show(ui, |ui| {
      self.add_input_ui(ui, selected);
    });
    let mut res = false;
    egui::CollapsingHeader::new("Output Texture").default_open(true).show(ui, |ui| {
      res = self.add_output_ui(ui, selected);
    });
    res
  }

  pub fn add_list_ui(&mut self, selected: &mut ShadyId, ui: &mut egui::Ui) {
    if selected.is_null() {
      return;
    }
    for (k,shady) in self.names.iter() {
      if ui.selectable_label(*selected == k, &shady.name).clicked() {
        *selected = k;
      }
      ui.separator();
    }
    ui.checkbox(&mut self.names[*selected].opt_scale_image, "scale texture");
  }

  pub fn add_input_ui(&mut self, ui: &mut egui::Ui, selected: ShadyId) {
    if selected.is_null() {
      return;
    }
    ui.label("Texture inputs from other shaders. Their binding layout will be in the order shown here.");
    let shady = &self.names[selected];
    let mut c = shady.inputs.clone(); // TODO: What's the idiom? How do I get around the stupid Rust checker.
    let mut changed = false;
    for (k,other) in self.names.iter() {
      if k == selected {
        continue;
      }

      let mut toggled = shady.inputs.contains(&k);
      if ui.checkbox(&mut toggled, &other.name).changed() {
        changed = true;
        if toggled {
          c.insert(k);
        } else {
          c.remove(&k);
        }
      }
    }
    if changed {
      self.names[selected].inputs = c;
      self.build_pipeline(selected);
    }
  }

  pub fn add_output_ui(&mut self, ui: &mut egui::Ui, selected: ShadyId) -> bool {
    if selected.is_null() {
      return false;
    }

    ui.label("Texture written into by the shader.");
    let shady = &mut self.names[selected];
    if shady.add_output_ui(ui) {
      let sz = [shady.ui_input_width.get(), shady.ui_input_height.get()];
      self.set_output(selected, sz, self.default_format);
    }
    true
  }

  pub fn add_central_ui(&mut self, ui: &mut egui::Ui, selected: ShadyId) {
    if selected.is_null() {
      ui.label("no shader selected");
      return;
    }

    if let Err(err) = &self.pipelines[selected] {
      ui.label(err);
    } else {
      let shady = &mut self.names[selected];
      shady.add_texture_ui(ui, self.egui_ids[selected]);
    }
  }
}

impl Shady {
  pub fn new(gpu: &Gpu, name: impl Into<String>, size: [u32;2], format: TextureFormat) -> Self {
    let texture = gpu.texture2d(size, format, TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING);
    let mut push_constants = PushValues::zeroed();

    push_constants.output_size = size;

    Self {
      name: name.into(),
      size: size,
      format: format,
      view: texture.create_view(&TextureViewDescriptor::default()),
      output: texture,
      inputs: HashSet::new(),
      tick_time: TickTime::default(),
      last_rendered: None,

      input_type: [InputType::Color, InputType::Color, InputType::Color, InputType::Color],
      push_constants,

      ui_input_width: IntInput::new("width", size[0], 16..4096, None),
      ui_input_height: IntInput::new("height", size[1], 16..4096, None),

      opt_scale_image: false,
    }
  }

  pub fn render(&mut self, encoder: &mut CommandEncoder, pipeline: &RenderPipeline, binds: &BindGroup) {
    if self.last_rendered == Some(self.tick_time.tick) && self.tick_time.is_paused() {
      return;
    }
    self.push_constants.time = self.tick_time.time as f32;
    self.push_constants.tick = self.tick_time.tick as i32;

    let mut pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
      label: None,
      depth_stencil_attachment: None,
      color_attachments: &[wgpu::RenderPassColorAttachment {
        view: &self.view,
        resolve_target: None,
        ops: wgpu::Operations {
          load: LoadOp::Clear(wgpu::Color::BLUE),
          store: true,
        },
      }],
    });

    pass.set_pipeline(pipeline);
    pass.set_bind_group(0, binds, &[]);
    pass.set_push_constants(
      ShaderStages::FRAGMENT,
      0,
      bytes_of(&self.push_constants)
    );
    pass.draw(0..6, 0..1);

    self.tick_time.advance();
    self.last_rendered = Some(self.tick_time.tick);
  }
}

impl Shady {
  pub fn set_output(&mut self, gpu: &Gpu, size: [u32;2], format: TextureFormat) {
    let texture = gpu.texture2d(size, format, TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING);
    self.size = size;
    self.format = format;
    self.view = texture.create_view(&TextureViewDescriptor::default());
    self.output = texture;
  }

  pub fn add_output_ui(&mut self, ui: &mut egui::Ui) -> bool {
    /// Returns true if output parameters changed.
    let mut changed = self.ui_input_width.ui(ui).changed();
    changed |= self.ui_input_height.ui(ui).changed();
    changed
  }

  pub fn add_input_ui(&mut self, ui: &mut egui::Ui) {
    for i in 0..4 {
      egui::CollapsingHeader::new(format!("input{}", i)).show(ui, |ui| {
        input_ui(ui, &mut self.input_type[i], &mut self.push_constants.input[i])
      });
    }
  }

  pub fn add_texture_ui(&mut self, ui: &mut egui::Ui, egui_id: egui::TextureId) {
    let size = if self.opt_scale_image {
      ui.available_size()
    } else { let [x,y] = self.size; egui::vec2(x as f32, y as f32) };

    let response = ui.add(egui::Image::new(egui_id, size).sense(
      egui::Sense {
        click: true,
        drag: true,
        focusable: true,
      }
    ).bg_fill(egui::Color32::BLUE));

    let corner = response.rect.min;
    // TODO: add mouse positions and stuff.
    if response.drag_started() {
      if let Some(ipos) = response.interact_pointer_pos() {
        self.push_constants.click_pos = (ipos - corner).into();
      }
      self.push_constants.drag_target = self.push_constants.click_pos;
    }
    if response.dragged() {
      self.push_constants.drag_target[0] += response.drag_delta().x;
      self.push_constants.drag_target[1] += response.drag_delta().y;
    }
    if let Some(hpos) = response.hover_pos() {
      self.push_constants.hover_pos = (hpos - corner).into();
    }

    self.push_constants.pointer_velocity = response.ctx.input().pointer.delta().into();
  }
}
