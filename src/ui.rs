//! Abstraction of EGUI.
//!
//! Module contains the trait [`UiContents`].
//!
pub use egui::{
  Ui, Response,
};

pub mod int_input;

pub trait UiContents {
  fn ui(&mut self, ui: &mut Ui) -> Option<Response>;
}

pub trait UiFrame {
  fn ui_top(&mut self, _ui: &mut Ui) {
  }

  fn ui_left(&mut self, _ui: &mut Ui) {
  }

  fn ui_central(&mut self, _ui: &mut Ui) {
  }
}
