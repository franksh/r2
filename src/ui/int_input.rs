use egui::{
  Ui, Widget,
  Response,
};
use ranges::{
  GenericRange,
  Domain,
};

pub trait SimpleInt: num::PrimInt + std::fmt::Display + std::str::FromStr + Domain {}
impl<T> SimpleInt for T where T: num::PrimInt + std::fmt::Display + std::str::FromStr + Domain {}

pub struct IntInput<T> {
  initial: T,
  value: T,
  work_buffer: String,
  range: GenericRange<T>,
  label: String,
  help: Option<String>,
}

impl<T: SimpleInt> IntInput<T> {
  pub fn new(label: &str, val: T, range: impl Into<GenericRange<T>>, help: Option<&str>) -> Self {
    Self {
      initial: val,
      value: val,
      range: range.into(),
      work_buffer: val.to_string(),
      label: label.to_owned(),
      help: help.map(|x| x.to_owned()),
    }
  }

  pub fn get(&self) -> T {
    self.value
  }

  pub fn set(&mut self, val: T) {
    self.value = val;
    self.work_buffer = val.to_string();
  }

  pub fn reset(&mut self) {
    self.set(self.initial);
  }
}

impl<T: SimpleInt> IntInput<T> {
  pub fn ui(&mut self, ui: &mut Ui) -> Response {
    let mut res = ui.horizontal(|ui| {
      ui.label(&self.label);
      let res = ui.text_edit_singleline(&mut self.work_buffer);
      if res.lost_focus() {
        if let Ok(val) = T::from_str(&self.work_buffer) {
          if self.range.contains(&val) {
            self.value = val;
            return true;
          }
        }
        self.reset();
      }
      if let Some(ref help) = self.help {
        res.on_hover_text(help);
      }
      false
    });
    if res.inner {
      res.response.mark_changed();
    }
    res.response
  }
}

